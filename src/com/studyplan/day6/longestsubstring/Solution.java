package com.studyplan.day6.longestsubstring;

/**
 * Given a string s, find the length of the longest substring without repeating characters.
 *
 * Input: s = "abcabcbb"
 * Output: 3
 * Explanation: The answer is "abc", with the length of 3.
 */
public class Solution {
    public int lengthOfLongestSubstring(String s) {
        int res = 0;

        for (int i = 0; i < s.length(); i++) {

            boolean[] visited = new boolean[256];
            for (int j = i; j < s.length(); j++) {

                if (visited[s.charAt(j)]) {
                    break;
                } else {
                    res = Math.max(res, j-i+1);
                    visited[s.charAt(j)] = true;
                }
            }
            visited[s.charAt(i)] = false;
        }

        return res;
    }
}
