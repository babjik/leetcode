package com.studyplan.day6.permutationstring;

import java.util.HashMap;
import java.util.Map;

public class Solution {
    public boolean checkInclusion(String s1, String s2) {
        if (s1 == null || s1.isEmpty()) {
            return false;
        }
        if (s2 == null || s2.isEmpty()) {
            return false;
        }
        for (int i = 0; i <= s2.length() - s1.length(); i++) {
            if (checkMatch(s1, s2.substring(i, i + s1.length()))) {
                return true;
            }
        }
        return false;
    }

    private boolean checkMatch(String s1, String s2) {
        System.out.println("Checking for s1 : " + s1 + " AND s2 : " + s2);
        Map<Character, Integer> map = new HashMap<>();

        for (char c : s2.toCharArray()) {
            Integer count = map.getOrDefault(c, 0);
            count++;
            map.put(c, count);
        }
        System.out.println(map);
        for (char c : s1.toCharArray()) {
            Integer count = map.get(c);
            if (!map.containsKey(c) || count == null || count == 0) {
                return false;
            }
            count--;
            map.put(c, count);
        }
        return true;
    }
}
