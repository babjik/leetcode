package com.studyplan.day6.permutationstring;

public class Test {
    public static void main(String[] args) {
        String s1 = "adc", s2 = "dcda";
        Solution s = new Solution();
        System.out.println(" " + s.checkInclusion(s1, s2));
    }
}
