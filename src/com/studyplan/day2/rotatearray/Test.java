package com.studyplan.day2.rotatearray;

import java.util.Arrays;

public class Test {
    public static void main(String[] args) {
        Solution s = new Solution();

        int[] nums = new int[]{1, 2, 3, 4, 5, 6};
        System.out.println("Actual:  " + Arrays.toString(nums));
        s.reverseRotate(nums, 1);

        System.out.println("Roteted: " + Arrays.toString(nums));
    }
}
