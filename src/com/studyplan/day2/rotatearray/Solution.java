package com.studyplan.day2.rotatearray;

public class Solution {
    public void rotate(int[] nums, int d) {
        int n = nums.length;
        d = d % n;
        int i, j, k, tmp;
        int gcd = gcd(d, n);
        for (i = 0; i < gcd; i++) {
            tmp = nums[i];
            j = i;
            while (true) {
                k = j + d;
                if (k >= n) {
                    k = k - n;
                }
                if (k == i)
                    break;
                nums[j] = nums[k];
                j = k;
            }
            nums[j] = tmp;
        }
    }


    public void reverseRotate(int[] nums, int d) {
        int n = nums.length;
        d = d % n;
        int i, j, k, tmp;
        int gcd = gcd(d, n);
        for (i = n - 1; i >= (n - gcd); i--) {
            tmp = nums[i];
            j = i;
            while (true) {
                k = j - d;
                if (k < 0) {
                    k = k + n;
                }
                if (k == i)
                    break;
                nums[j] = nums[k];
                j = k;
            }
            nums[j] = tmp;
        }
    }


    public int gcd(int a, int b) {
        if (b == 0) {
            return a;
        }
        return gcd(b, a % b);
    }
}
