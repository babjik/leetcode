package com.studyplan.day2.squaresofsortedarray;

import java.util.Arrays;

public class Test {
    public static void main(String[] args) {
        Solution s = new Solution();
        System.out.println(" " + Arrays.toString(s.sortedSquares(new int [] {-4,-1,0,3,10})));
    }
}
