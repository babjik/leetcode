package com.studyplan.day2.squaresofsortedarray;

import java.util.Arrays;

public class Solution {
    public int[] sortedSquares(int[] nums) {
        int[] sqrs = new int[nums.length];

        int index = 0;
        for (int num : nums) {
            int sqr = num * num;
            insertVal(sqrs, index++, sqr);
        }

        return sqrs;
    }

    private void insertVal(int[] sqrs, int n, int value) {
        if (n == 0) {
            sqrs[n] = value;
            return;
        }
        int i = n -1;
        while (i >= 0 && sqrs[i] > value) {
            sqrs[i + 1] = sqrs[i];
            i--;
        }
        sqrs[i + 1] = value;
    }
}
