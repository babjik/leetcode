package com.studyplan.day7.floodfill;

import com.sample.leetcode.utils.Pair;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 * An image is represented by an m x n integer grid image where image[i][j] represents the pixel value of the image.
 *
 * You are also given three integers sr, sc, and newColor. You should perform a flood fill on the image starting from the pixel image[sr][sc].
 *
 * To perform a flood fill, consider the starting pixel, plus any pixels connected 4-directionally to the starting pixel of the same color as the starting pixel, plus any pixels connected 4-directionally to those pixels (also with the same color), and so on. Replace the color of all of the aforementioned pixels with newColor.
 *
 * Return the modified image after performing the flood fill.
 */
public class Solution {

    // Direction vectors
    static int dRow[] = { -1, 0, 1, 0 };
    static int dCol[] = { 0, 1, 0, -1 };

    public int[][] floodFill(int[][] image, int sr, int sc, int newColor) {
        boolean[][] vis = new boolean[image.length][image[0].length];
        Queue<Pair<Integer, Integer>> q = new PriorityQueue<>(image.length * image[0].length, Comparator.comparing(Pair::getKey));
        q.add(new Pair<>(sr, sc));
        int old = image[sr][sc];
        while (!q.isEmpty()) {
            final Pair<Integer, Integer> current = q.peek();
            final Integer x = current.getKey();
            final Integer y = current.getValue();

            image[x][y] = newColor;
            q.remove();
            for (int i = 0; i < 4; i++) {
                int adjx = x + dRow[i];
                int adjy = y + dCol[i];

                if (isValid(image, vis, adjx, adjy, old)) {
                    q.add(new Pair<>(adjx, adjy));
                    vis[adjx][adjy] = true;
                }
            }
        }
        return image;
    }

    private boolean isValid(int[][] image, boolean[][] vis, int i, int j, int old) {

        if (i < 0 || j < 0 || image.length <= i || image[0].length <= j) {
            return false;
        }
        if (vis[i][j]) {
            return false;
        }

        if (image[i][j] != old) {
            return false;
        }
        return true;
    }
}
