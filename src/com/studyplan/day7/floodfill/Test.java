package com.studyplan.day7.floodfill;

import java.util.Arrays;

public class Test {
    public static void main(String[] args) {
//        int[][] image = new int[][] {{1,1}, {1,1}, {1,0}};
//        print(image);
//        Solution s = new Solution();
//        System.out.println("-----------------------");
//        print(s.floodFill(image, 1, 1, 2));


        int[][] image = new int[][] {{0, 0, 0}, {0, 1, 0}};
        print(image);
        Solution s = new Solution();
        System.out.println("-----------------------");
        print(s.floodFill(image, 1, 0, 2));
    }

    private static void print(int[][] image) {
        for (int i = 0; i < image.length; i++) {
            for (int j = 0; j < image[0].length; j++) {
                System.out.printf(image[i][j] + ", ");
            }
            System.out.println("");
        }
    }
}
