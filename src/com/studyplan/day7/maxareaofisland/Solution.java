package com.studyplan.day7.maxareaofisland;

/**
 * You are given an m x n binary matrix grid. An island is a group of 1's (representing land) connected 4-directionally (horizontal or vertical.) You may assume all four edges of the grid are surrounded by water.
 * The area of an island is the number of cells with a value 1 in the island.
 * Return the maximum area of an island in grid. If there is no island, return 0.
 */
public class Solution {
    public int maxAreaOfIsland(int[][] grid) {
        boolean[][] vis = new boolean[grid.length][grid[0].length];
        int max = 0;

        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                if (isValid(grid, vis, i, j)) {
                    max = Math.max(max, getIsLandSize(grid, vis, i, j));
                }
                vis[i][j] = true;
            }
        }
        
        return max;
    }

    static int dRow[] = { 0, -1, 0, 1, 0 };
    static int dCol[] = { 0, 0, 1, 0, -1 };
    private int getIsLandSize(int[][] grid, boolean[][] vis, int i, int j) {
        int size = 0;
        for (int k = 0; k < 5; k++) {
            int adjx = i + dRow[k];
            int adjy = j + dCol[k];

            if (isInBoundary(grid, adjx, adjy) && grid[adjx][adjy] == 1 && !vis[adjx][adjy]) {
                size++;
                vis[adjx][adjy] = true;
                size += getIsLandSize(grid, vis, adjx, adjy);
            }
        }
        return size;
    }

    private boolean isInBoundary(int[][] grid, int i, int j) {
        return i >= 0 && j >= 0 && i < grid.length && j < grid[0].length;
    }

    private boolean isValid(int[][] grid, boolean[][] vis, int i, int j) {
        if (vis[i][j] || grid[i][j] == 0) {
            return false;
        }
        return true;
    }
}
