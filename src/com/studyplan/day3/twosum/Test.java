package com.studyplan.day3.twosum;

import java.util.Arrays;

public class Test {
    public static void main(String[] args) {

        Solution s = new Solution();
        System.out.println(Arrays.toString(s.twoSum(new int[]{2,7,11,15}, 9)));
    }
}
