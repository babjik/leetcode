package com.studyplan.day3.movezeros;

/**
 * Given an integer array nums, move all 0's to the end of it while maintaining the relative order of the non-zero elements.
 *
 * Note that you must do this in-place without making a copy of the array.
 */
public class Solution {
    public void moveZeroes(int[] nums) {
        int n = nums.length;
        int insert = 0, cursor = 0;

        while (cursor < n) {
            if (nums[cursor] != 0) {
                int tmp = nums[cursor];
                nums[cursor] = nums[insert];
                nums[insert] = tmp;
                insert++;
            }
            cursor++;
        }

        while (insert < n) {
            nums[insert++] = 0;
        }
    }

}
