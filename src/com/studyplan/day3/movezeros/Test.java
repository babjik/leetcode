package com.studyplan.day3.movezeros;

import java.util.Arrays;

public class Test {
    public static void main(String[] args) {
        Solution s = new Solution();

        int[] nums = new int[] {-1, 0,1,0,3,12};
        System.out.println("Input : " + Arrays.toString(nums));
        s.moveZeroes(nums);
        System.out.println("roted : " + Arrays.toString(nums));

    }
}
