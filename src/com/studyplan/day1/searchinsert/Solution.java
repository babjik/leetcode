package com.studyplan.day1.searchinsert;

import java.util.Arrays;

/**
 * Given a sorted array of distinct integers and a target value, return the index if the target is found. If not, return the index where it would be if it were inserted in order.
 *
 * You must write an algorithm with O(log n) runtime complexity.
 */
public class Solution {
    public int searchInsert(int[] nums, int target) {
        System.out.println("Input " + Arrays.toString(nums) + " : target - " + target);
        int left = 0;
        int right = nums.length -1;
        int mid = 0;
        while (left <= right) {
            mid = left + (right - left) / 2;
            System.out.println("l - " + left + " : r - " + right + " : m - " + mid + " : val " + nums[mid]);
            if (nums[mid] == target) {
                return mid;
            } else if (nums[mid] > target) {
                right = mid - 1;
            } else if (nums[mid] < target) {
                left = mid + 1 ;
            }
        }
        return (nums[mid] > target) ? mid : mid + 1 ; // if in case no place found, will insert at end
    }
}
