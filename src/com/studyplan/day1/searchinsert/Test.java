package com.studyplan.day1.searchinsert;

public class Test {
    public static void main(String[] args) {
        Solution s = new Solution();
        System.out.println("2 - " + s.searchInsert(new int[] {1,3,5,6}, 5));
        System.out.println("1 - " + s.searchInsert(new int[] {1,3,5,6}, 2));
        System.out.println("4 - " + s.searchInsert(new int[] {1,3,5,6}, 7));
        System.out.println("0 - " + s.searchInsert(new int[] {1,3,5,6}, 0));
    }
}
