package com.studyplan.day1.binerysearch;

public class Solution {
    public int search(int[] nums, int target) {

        return  search(nums, 0, nums.length -1, target);
    }

    private int search(int[] nums, int l, int r, int target) {
        if (r >= l) {
            int mid = l + (r - l) / 2;

            if (nums[mid] == target)
                return mid;

            if (nums[mid] > target)
                return search(nums, l, mid - 1, target);

            return search(nums, mid + 1, r, target);
        }
        return -1;
    }
}
