package com.studyplan.day1.binerysearch;

import java.util.Arrays;

public class Test {
    public static void main(String[] args) {
        Solution s = new Solution();

        System.out.println(" " + s.search(new int[]{-1, 0, 3, 5, 9, 12}, 9));
        System.out.println(" " + s.search(new int[]{-1, 0, 3, 5, 9, 12}, 2));
    }
}
