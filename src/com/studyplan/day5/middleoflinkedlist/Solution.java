package com.studyplan.day5.middleoflinkedlist;

import com.sample.leetcode.utils.ListNode;

/**
 * Given the head of a singly linked list, return the middle node of the linked list.
 *
 * If there are two middle nodes, return the second middle node.
 */
public class Solution {
    public ListNode middleNode(ListNode head) {
        ListNode oneStepPtr = head;
        ListNode twoStepPtr = head;

        while (twoStepPtr != null && twoStepPtr.next != null && twoStepPtr.next.next != null) {
            oneStepPtr = oneStepPtr.next;
            twoStepPtr = twoStepPtr.next.next;
        }

        if (twoStepPtr != null && twoStepPtr.next != null) {
            oneStepPtr = oneStepPtr.next;
        }

        return oneStepPtr;
    }
}
