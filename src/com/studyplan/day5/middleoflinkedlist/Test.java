package com.studyplan.day5.middleoflinkedlist;

import com.sample.leetcode.utils.ListNode;
import com.sample.leetcode.utils.NodeUtils;


public class Test {
    public static void main(String[] args) {
        Solution s = new Solution();
        ListNode listNode = NodeUtils.buildNode(new int[]{1, 2, 3, 4, 5});
        NodeUtils.printListNode(listNode);
        System.out.println("middle " + s.middleNode(listNode).val);

        listNode = NodeUtils.buildNode(new int[]{1, 2, 3, 4, 5, 6});
        NodeUtils.printListNode(listNode);
        System.out.println("middle " + s.middleNode(listNode).val);
    }
}
