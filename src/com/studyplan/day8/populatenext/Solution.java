package com.studyplan.day8.populatenext;

import com.sample.leetcode.utils.Node;

/**
 * https://leetcode.com/problems/populating-next-right-pointers-in-each-node/
 * 116. Populating Next Right Pointers in Each Node
 */
public class Solution {
    public Node connect(Node root) {
        if (root == null)
            return null;
        connect(root.left, root.right);
        return root;
    }

    private void connect(Node left, Node right) {
        if (left == null)
            return;
        left.next = right;
        connect(left.left, left.right);
        connect(right.left, right.right);
        connect(left.right, right.left);
    }
}
