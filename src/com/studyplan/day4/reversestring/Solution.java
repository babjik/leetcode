package com.studyplan.day4.reversestring;

/**
 * Write a function that reverses a string. The input string is given as an array of characters s.
 *
 * You must do this by modifying the input array in-place with O(1) extra memory.
 *
 *
 * two pointer - algo
 */
public class Solution {
    public void reverseString(char[] s) {
        int i = 0, j = s.length -1;

        while (i < j) {
            char tmp = s[i];
            s[i] = s[j];
            s[j] = tmp;

            i++;
            j--;
        }
    }
}
