package com.studyplan.day4.reversestring;

import java.util.Arrays;

public class Test {
    public static void main(String[] args) {

        Solution s = new Solution();
        String str = "hello";
        s.reverseString(str.toCharArray());
        System.out.println(str);
    }
}
