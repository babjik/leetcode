package com.studyplan.day4.reversewords;

/**
 * Given a string s, reverse the order of characters in each word within a sentence while still preserving whitespace and initial word order.
 */
public class Solution {


    public String reverseWords(String s) {
        int start = 0, end = 0, i = 0;
        char[] chars =  s.toCharArray();
        for (; i < chars.length; i++) {
            if (chars[i] - ' ' == 0) {
                end = i - 1;
                reverse(chars, start, end);
                start = i + 1;
            }
        }
        // for last word
        if (end == 0 && start == 0) {
            reverse(chars, 0, chars.length -1);
        }  else {
            reverse(chars, end + 2, chars.length -1);
        }
        return String.valueOf(chars);
    }

    public void reverse(char[] chars, int start, int end) {
        while (start < end) {
            char tmp = chars[start];
            chars[start] = chars[end];
            chars[end] = tmp;

            start++;
            end--;
        }
    }
}
