package com.studyplan.day4.reversewords;

public class Test {
    public static void main(String[] args) {
        Solution s = new Solution();
        System.out.println("Let's take LeetCode contest :  " + s.reverseWords("Let's take LeetCode contest"));
        System.out.println("contest : " + s.reverseWords("contest"));
    }
}
