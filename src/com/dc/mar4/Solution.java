package com.dc.mar4;

import java.util.ArrayList;
import java.util.List;

/**
 * 799. Champagne Tower
 * <p>
 * <p>
 * We stack glasses in a pyramid, where the first row has 1 glass, the second row has 2 glasses, and so on until the 100th row.  Each glass holds one cup of champagne.
 * <p>
 * Then, some champagne is poured into the first glass at the top.  When the topmost glass is full, any excess liquid poured will fall equally to the glass immediately to the left and right of it.  When those glasses become full, any excess champagne will fall equally to the left and right of those glasses, and so on.  (A glass at the bottom row has its excess champagne fall on the floor.)
 * <p>
 * For example, after one cup of champagne is poured, the top most glass is full.  After two cups of champagne are poured, the two glasses on the second row are half full.  After three cups of champagne are poured, those two cups become full - there are 3 full glasses total now.  After four cups of champagne are poured, the third row has the middle glass half full, and the two outside glasses are a quarter full, as pictured below.
 */
public class Solution {
    public double champagneTower(int poured, int query_row, int query_glass) {
        double[][] arr = new double[query_row + 2][query_glass + 2];
        arr[0][0] = poured;
        for (int r = 0; r <= query_row; r++) {
            for (int c = 0; c <= query_glass; c++) {
                double temp = (arr[r][c] - 1.0) / 2;
                if (temp > 0) {
                    arr[r + 1][c] += temp;
                    arr[r + 1][c + 1] += temp;
                }

            }
        }
        return Math.min(1, arr[query_row][query_glass]);
    }
}
