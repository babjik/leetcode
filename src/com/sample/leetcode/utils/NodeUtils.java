package com.sample.leetcode.utils;

public class NodeUtils {
    public static ListNode buildNode(int[] arr) {
        ListNode dummy = new ListNode(0), curr;
        curr = dummy;
        for (int i : arr) {
            curr.next = new ListNode(i);
            curr = curr.next;
        }
        return dummy.next;
    }

    public static void printListNode(ListNode head) {
        ListNode curr = head;
        StringBuilder sb = new StringBuilder("[");
        while (curr != null) {
            sb.append(curr.val);

            curr = curr.next;
            if (curr != null) {
                sb.append(" -> ");
            }
        }
        sb.append("]");
        System.out.println(sb);
    }
}
