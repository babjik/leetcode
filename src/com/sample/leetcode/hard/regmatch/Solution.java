package com.sample.leetcode.hard.regmatch;

public class Solution {
    public boolean isMatch(String s, String p) {
        if (p.equals(".*")) {
            return true;
        }
        int pPos = 0;
        int strPos = 0;
        while (pPos < p.length() && strPos < s.length()) {
            if (p.charAt(pPos) == '.') {
                strPos++;
            } else if (Character.isAlphabetic(p.charAt(pPos)) && p.length() > pPos + 1 && p.charAt(pPos + 1) == '*') {
                while (strPos < s.length() && pPos < p.length() &&  s.charAt(strPos) == p.charAt(pPos)) {
                    strPos++;
                }
                pPos++;
            } else if (Character.isAlphabetic(p.charAt(pPos))) {
                if (s.charAt(strPos) != p.charAt(pPos)) {
                    return false;
                }
                strPos++;
            }
            pPos++;
        }
        return strPos >= s.length() && pPos >= p.length();
    }
}
