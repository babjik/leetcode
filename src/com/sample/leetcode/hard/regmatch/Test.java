package com.sample.leetcode.hard.regmatch;

public class Test {

    /*
    *
        Given an input string s and a pattern p, implement regular expression matching with support for '.' and '*' where:
        '.' Matches any single character.
        '*' Matches zero or more of the preceding element.
        The matching should cover the entire input string (not partial).

    * */
    public static void main(String[] args) {
        Solution s = new Solution();

//        System.out.println("false : " + s.isMatch("aa", "a"));
//        System.out.println("true : " + s.isMatch("aa", "a*"));
//        System.out.println("true : " + s.isMatch("ab", ".*"));
//        System.out.println("true : " + s.isMatch("aab", "c*a*b"));
//        System.out.println("false : " + s.isMatch("mississippi", "mis*is*p*."));
        System.out.println("true : " + s.isMatch("aaa", "a*a"));
    }
}
