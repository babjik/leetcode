package com.sample.leetcode.hard.longestpalindromicsubstring;

import java.util.HashMap;
import java.util.Map;

public class Solution {
    public String longestPalindrome(String s) {
        if (s == null || s.length() < 1)
            return "";

        int start = 0, maxLength = 1;
        Map<String, Boolean> checked = new HashMap<>();
        for (int i = 0; i < s.length(); i++) {

            for (int j = i; j < s.length(); j++) {
                boolean flag = true;

                String key = i + "-" + j;

                for (int k = 0 ; k < (j - i + 1) / 2; k++) {
                    if (s.charAt(i+k) != s.charAt(j-k)) {
                        flag = false;
                        break;
                    }
                }

                if (flag && (j - i + 1) > maxLength) {
                    start = i;
                    maxLength = j - i + 1;

                }

            }

        }
        return s.substring(start, start + maxLength);
    }


}

