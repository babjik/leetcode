package com.sample.leetcode.hard.mergeksortedlists;

import com.sample.leetcode.utils.ListNode;

import java.util.Comparator;
import java.util.PriorityQueue;

public class Solution {
    public ListNode mergeKLists(ListNode[] lists) {
        if (lists == null || lists.length == 0) {
            return null;
        }

        PriorityQueue<ListNode> queue = new PriorityQueue<>(lists.length, Comparator.comparingInt((ListNode a) -> a.val));

        for (ListNode node : lists) {
            if (node != null)
                queue.offer(node);
        }

        ListNode dummy = new ListNode(0), tail = dummy;

        while (!queue.isEmpty()) {
            ListNode curr = queue.poll();

            tail.next = curr;
            tail = tail.next;

            if (curr.next != null) {
                queue.offer(curr.next);
            }
        }
        return dummy.next;
    }
}
