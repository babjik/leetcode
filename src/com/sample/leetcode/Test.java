package com.sample.leetcode;


import com.sample.leetcode.medium.stringtoint.Solution;

public class Test {
    public static void main(String[] args) {
        Solution s = new Solution();

        System.out.println("09 : " + s.myAtoi("09"));
        System.out.println("42 : " + s.myAtoi("42"));
        System.out.println("42 : " + s.myAtoi("+42"));
        System.out.println("-42 : " + s.myAtoi("   -42"));
        System.out.println("4193 : " + s.myAtoi("4193 with words"));
        System.out.println("0 : " + s.myAtoi("words and 987"));
        System.out.println("-2147483648 : " + s.myAtoi("-91283472332"));
        System.out.println("3 : " + s.myAtoi("3.14159"));
        System.out.println("0 : " + s.myAtoi("+-12.04"));
        System.out.println("-12 : " + s.myAtoi("  -0012a42"));
        System.out.println("0 : " + s.myAtoi("00000-42a1234"));
        System.out.println("0 : " + s.myAtoi("   +0 123"));
        System.out.println("0 : " + s.myAtoi("    +0a32"));
        System.out.println("2147483647 : " + s.myAtoi("2147483648"));
        System.out.println("2147483646 : " + s.myAtoi("2147483646"));
    }
}
