package com.sample.leetcode.medium.swapnodesinpairs;

import com.sample.leetcode.utils.ListNode;
import com.sample.leetcode.utils.NodeUtils;

public class Test {
    public static void main(String[] args) {

        ListNode head = NodeUtils.buildNode(new int[] {1,2,3,4});
        NodeUtils.printListNode(head);
        Solution s = new Solution();
        ListNode swapped = s.swapPairs(head);
        NodeUtils.printListNode(swapped);

    }
}
