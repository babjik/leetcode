package com.sample.leetcode.medium.swapnodesinpairs;

import com.sample.leetcode.utils.ListNode;

public class Solution {
    public ListNode swapPairs(ListNode head) {
        ListNode dummy = new ListNode(0), curr = head, prev, thisGuy, nextGuy, tailPart;
        dummy.next = head;
        prev = dummy;
        while (curr != null && curr.next != null) {
            nextGuy = curr.next;

            tailPart = nextGuy.next;

            prev.next = nextGuy;
            nextGuy.next = curr;
            curr.next = tailPart;

            curr = tailPart;
            prev = prev.next.next;
        }
        return dummy.next;
    }
}
