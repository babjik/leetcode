package com.sample.leetcode.medium.threesum;


public class Test {

    public static void main(String[] args) {
        Solution s = new Solution();

//        System.out.println(" [-1,0,1,2,-1,-4] -- " + s.threeSum(new int[] {-1,0,1,2,-1,-4}));
//        System.out.println(" []-- " + s.threeSum(new int[] {}));
//        System.out.println(" [0] -- " + s.threeSum(new int[] {0}));
        System.out.println(" [-2,0,1,1,2] -- " + s.threeSum(new int[] {-2,0,1,1,2}));
    }
}
