package com.sample.leetcode.medium.threesum;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Solution {
    public List<List<Integer>> threeSum(int[] nums) {

        Arrays.sort(nums);
        Set<List<Integer>> result = new HashSet<>();

        if (nums.length < 3) {
            return new ArrayList<>();
        }

        for (int i = 0; i < nums.length - 2; i++) {
            int j = i+1;
            int k = nums.length - 1;

            while (j < k) {
                int sum = nums[i] + nums[j] + nums[k];
                if (sum == 0) {
                    result.add(Arrays.asList(nums[i], nums[j], nums[k]));
                    while(j<k && nums[j] == nums[j+1]) j++;
                    while(j<k && nums[k] == nums[k-1]) k--;
                    j++;
                    k--;
                } else if (sum > 0){
                    k--;
                } else
                    j++;
            }
        }
        return new ArrayList<>(result);
    }
}
