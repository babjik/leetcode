package com.sample.leetcode.medium.stringtoint;

public class Solution {
    public int myAtoi(String s) {
        // if (asci == 43 || asci == 45) {
        long res = 0;
        int multiplayer = 1;
        boolean symbol = false;

        for (char c : s.trim().toCharArray()) {
            if (c == '-' || c == '+') {
                if (symbol) {
                    break;
                }
                if (c == '-') {
                    multiplayer = -1;
                }
                symbol = true;
            } else if (Character.isDigit(c)) {
                symbol = true;
                res = res * 10 + ((int) c - '0');
            } else {
                break;
            }

            if (res * multiplayer >= Integer.MAX_VALUE){
                return Integer.MAX_VALUE;
            } else if (res * multiplayer <= Integer.MIN_VALUE) {
                return Integer.MIN_VALUE;
            }
        }

        return  (int)res * multiplayer;
    }
}
