package com.sample.leetcode.medium.containerwithmostwater;

public class Test {
    public static void main(String[] args) {
        Solution s = new Solution();
        assert 49 == s.maxArea(new int[] {1,8,6,2,5,4,8,3,7}) : "Failed";
        assert 1 == s.maxArea(new int[] {1,1}) : "Failed";
    }
}
