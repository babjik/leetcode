package com.sample.leetcode.medium.closetsum;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Solution {
    public int threeSumClosest(int[] nums, int target) {
        if(nums.length == 3) {
            return nums[0] + nums[1] + nums[2];
        }
        Arrays.sort(nums);
        int closestSum = Integer.MAX_VALUE;

        for(int i = 0; i <= nums.length - 3; i++) {
            int j = i + 1, k = nums.length - 1;
            int sum = 0;
            while(j < k) {
                sum = nums[i] + nums[j] + nums[k];
                if(sum == target) {
                    return sum;
                } else if(closestSum == Integer.MAX_VALUE || Math.abs(target - sum) < Math.abs(target - closestSum)) {
                    closestSum = sum;
                }

                if(sum < target) j++;
                else k--;
            }
        }

        return closestSum;
    }
}
