package com.sample.leetcode.medium.removenthnodefrmlast;

import com.sample.leetcode.utils.ListNode;

public class Solution {
    public ListNode removeNthFromEnd(ListNode head, int n) {
        if(head.next == null) return null;
        ListNode slowPtr = head, fastPtr = head;

        for(int i = 0; i < n; i++){
            fastPtr = fastPtr.next;
        }

        if(fastPtr == null) return slowPtr.next;

        while(fastPtr.next != null){
            slowPtr = slowPtr.next;
            fastPtr = fastPtr.next;
        }
        slowPtr.next = slowPtr.next.next;
        return head;
    }
}
