package com.sample.leetcode.medium.zigzag;

import java.util.ArrayList;
import java.util.List;

public class Solution {
    public String convert(String s, int numRows) {
        StringBuilder res = new StringBuilder();
        List<StringBuilder> rows = new ArrayList<>();

        for (int i = 0; i < Math.max(numRows, s.length()); i++) {
            rows.add(new StringBuilder());
        }
        boolean down = true;
        int currRow = 0;
        for (char c : s.toCharArray()) {
            rows.get(currRow).append(c);

            if (currRow == 0)
                down = true;
            else if (currRow == numRows -1)
                down = false;

            currRow += down ? 1 : -1;
        }
        for (StringBuilder row : rows) {
            res.append(row);
        }
        return res.toString();
    }
}
