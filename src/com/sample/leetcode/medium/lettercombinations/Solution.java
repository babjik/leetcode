package com.sample.leetcode.medium.lettercombinations;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Solution {
    static Map<Character, String> map = new HashMap<>();

    static  {
        map.put('2', "abc");
        map.put('3', "def");
        map.put('4', "ghi");
        map.put('5', "jkl");
        map.put('6', "mno");
        map.put('7', "pqrs");
        map.put('8', "tuv");
        map.put('9', "wxyz");
    }

    public List<String> letterCombinations(String digits) {

        return letterCombinations("", digits);
    }


    public List<String> letterCombinations(String s, String digits) {
        List<String> list = new ArrayList<>();
        if (digits.isEmpty()) {
            list.add(s);
            return list;
        }

        Character firstChar = digits.charAt(0);
        String chars = map.get(firstChar);

        for (int i = 0; i < chars.toCharArray().length; i++) {
            String tmp = s + chars.charAt(i);
            list.addAll(letterCombinations(tmp, digits.substring(1)));
        }

        return list;
    }
}
