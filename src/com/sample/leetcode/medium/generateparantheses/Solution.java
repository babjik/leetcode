package com.sample.leetcode.medium.generateparantheses;

import java.util.ArrayList;
import java.util.List;

/**
 * Given n pairs of parentheses, write a function to generate all combinations of well-formed parentheses.
 *
 * Input: n = 3
 * Output: ["((()))","(()())","(())()","()(())","()()()"]
 *
 *
 * Input: n = 1
 * Output: ["()"]
 */
public class Solution {
    private void solve(String str, int open, int close, List<String> list) {
        if (open == 0 && close == 0) {
            list.add(str);
            return;
        }

        if (open > 0) {
            solve(str + "(", open - 1, close, list);
        }

        if (open < close && close > 0) {
            solve(str + ")", open, close - 1, list);
        }
    }
    public List<String> generateParenthesis(int n) {
        List<String> list = new ArrayList<>();
        solve("", n, n, list);
        return list;
    }
}
