package com.sample.leetcode.medium.reverseint;

public class Solution {
    public int reverse(int x) {
        System.out.println("X " + x);
        int res = 0;
        do {
            if (res > Integer.MAX_VALUE / 10) return 0;

            if ( res < Integer.MIN_VALUE / 10) return 0;

            res = res * 10 + x % 10;
            x = x / 10;

        } while (x / 10 != 0 || x % 10 != 0);
        return res;
    }
}
