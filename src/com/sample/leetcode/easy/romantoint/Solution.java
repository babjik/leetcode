package com.sample.leetcode.easy.romantoint;

public class Solution {
    public int romanToInt(String s) {
        String[] romans = new String[] {"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"};
        int[] numbers = new int[] {1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};
        int value = 0;

        while (!s.isEmpty()) {
            for (int i = 0; i < romans.length; i++) {
                if (s.startsWith(romans[i])) {
                    value += numbers[i];
                    s = s.replaceFirst(romans[i], "");
                }
            }
        }

        return value;
    }
}
