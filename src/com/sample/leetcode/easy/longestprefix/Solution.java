package com.sample.leetcode.easy.longestprefix;

public class Solution {
    public String longestCommonPrefix(String[] strs) {
        String prefix = strs[0];

        for (int i = 1; i < strs.length; i++) {

            final String str = strs[i];

            for (int j = 0; j < str.length() && j < prefix.length() ; j++) {
                if (str.charAt(j) != prefix.charAt(j)) {
                    prefix = str.substring(0, j);
                    break;
                }
            }
            if (str.length() < prefix.length()) {
                prefix = str;
            }
        }

        return prefix;
    }
}
