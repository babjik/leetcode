package com.sample.leetcode.easy.longestprefix;

public class Test {
    public static void main(String[] args) {
        Solution s = new Solution();
        System.out.println(s.longestCommonPrefix(new String[] {"flower", "flow", "flight"}));
        System.out.println(s.longestCommonPrefix(new String[] {"dog","racecar","car"}));
        System.out.println(s.longestCommonPrefix(new String[] {"ab","a"}));

        assert "".equals(s.longestCommonPrefix(new String[] {"dog","racecar","car"})) : "Failed";
    }
}
