package com.sample.leetcode.easy.medianofsortedarrays;

public class Solution {
    public double findMedianSortedArrays(int[] nums1, int[] nums2) {
        int i = 0, j = 0, k = 0;
        int[] arr = new int[nums1.length + nums2.length + 10];

        while (i < nums1.length || j < nums2.length) {
            if (i >= nums1.length) {
                arr[k] = nums2[j];
                j++;
            } else if (j >= nums2.length) {
                arr[k] = nums1[i];
                i++;
            } else if (nums1[i] < nums2[j]) {
                arr[k] = nums1[i];
                i++;
            } else {
                arr[k] = nums2[j];
                j++;
            }
            k++;
        }

        if (k == 0) {
            return 0;
        } else if ((k) % 2 == 1) {
            return arr[k/2];
        } else {
            return (arr[k/2] + arr[k/2 - 1]) / 2.0;
        }
    }
}
