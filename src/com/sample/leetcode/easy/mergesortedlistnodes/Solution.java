package com.sample.leetcode.easy.mergesortedlistnodes;

import com.sample.leetcode.utils.ListNode;

public class Solution {
    public ListNode mergeTwoLists(ListNode list1, ListNode list2) {
        ListNode result = new ListNode();

        while (list1 != null || list2 != null) {
            if (list1.val < list2.val) {
                result.next = list1;
                list1 = list1.next;
            } else {
                result.next = list2;
                list2 = list2.next;
            }
            result = result.next;
        }

        if (list2 != null) {
            result.next = list2;
        }

        if (list1 != null) {
            result.next = list1;
        }

        return result.next;
    }
}
