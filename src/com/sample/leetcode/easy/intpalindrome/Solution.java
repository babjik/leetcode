package com.sample.leetcode.easy.intpalindrome;

public class Solution {
    public boolean isPalindrome(int x) {
        if (x < 0) {
            return false;
        } else if (x < 10) {
            return true;
        }
        long input = x;
        long palindrome = 0;

        while (input > 0) {
            palindrome = palindrome * 10 + input % 10;

            input /= 10;
        }
        return x == palindrome;
    }
}
