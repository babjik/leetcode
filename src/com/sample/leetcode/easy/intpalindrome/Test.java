package com.sample.leetcode.easy.intpalindrome;

public class Test {
    public static void main(String[] args) {
        Solution s = new Solution();

        System.out.println("true : " + s.isPalindrome(121));
        System.out.println("false : " + s.isPalindrome(-121));
        System.out.println("false : " + s.isPalindrome(10));
        System.out.println("false : " + s.isPalindrome(-101));
    }
}
