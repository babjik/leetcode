package com.sample.leetcode.easy.validparantheses;

import java.util.Arrays;
import java.util.Stack;

public class Solution {

    public boolean isValid(String s) {
        Stack<Character> stack = new Stack<>();

        for (char c : s.toCharArray()) {

            if (Arrays.asList('(', '{', '[').contains(c)) {
                stack.push(c);
            } else {
                if (stack.isEmpty()) {
                    return false;
                } else if (!check(stack.pop(), c)) {
                    return false;
                }
            }
        }

        return stack.isEmpty();
    }

    private boolean check(char a, char b) {
        return a == '(' && b == ')'
            || a == '{' && b == '}'
            || a == '[' && b == ']';
    }
}
