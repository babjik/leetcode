package com.techgig;

import java.io.*;
import java.util.*;
public class CandidateCode {
    public static void main(String args[] ) throws Exception {

        //Write code here
        Scanner myObj = new Scanner(System.in);
        int size = myObj.nextInt();
        Set<Integer> rolls = new HashSet<>();

        for (int i = 0; i < size; i++) {
            rolls.add(myObj.nextInt());
        }

        for (int i = 1; i <= size; i++) {
            if (!rolls.contains(i)) {
                System.out.print(i + " ");
            }
        }
    }
}
